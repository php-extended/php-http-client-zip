<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-zip library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\HttpClient;

use InvalidArgumentException;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Psr\Http\Message\StreamInterface;
use RuntimeException;
use Stringable;
use ZipArchive;

/**
 * ZipClient class file.
 * 
 * This clas is an implementation of a client which does handle compressed
 * responses from servers. It also modifies requests to specifically allow the
 * server to send their response compressed.
 * 
 * @author Anastaszor
 */
class ZipClient implements ClientInterface, Stringable
{
	
	/**
	 * The inner client.
	 *
	 * @var ClientInterface
	 */
	protected ClientInterface $_client;
	
	/**
	 * The stream factory.
	 *
	 * @var StreamFactoryInterface
	 */
	protected StreamFactoryInterface $_streamFactory;
	
	/**
	 * Builds a new CompressorProcessor with the given inner processor.
	 *
	 * @param ClientInterface $client
	 * @param StreamFactoryInterface $streamFactory
	 */
	public function __construct(ClientInterface $client, StreamFactoryInterface $streamFactory)
	{
		$this->_client = $client;
		$this->_streamFactory = $streamFactory;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Psr\Http\Client\ClientInterface::sendRequest()
	 */
	public function sendRequest(RequestInterface $request) : ResponseInterface
	{
		$request = $this->preProcess($request);
		
		$response = $this->_client->sendRequest($request);
		
		try
		{
			return $this->postProcess($response);
		}
		catch(RuntimeException $exc)
		{
			throw new ZipClientException($request, $response, 'Failed to unzip the file from '.$request->getUri()->__toString(), -1, $exc);
		}
	}
	
	/**
	 * Pre-processes the request by adding the right headers to inform the
	 * target server that we accept compressed data.
	 *
	 * @param RequestInterface $request
	 * @return RequestInterface
	 */
	protected function preProcess(RequestInterface $request) : RequestInterface
	{
		
		$acceptEncodings = $request->getHeaderLine('Accept-Encoding');
		/** @phpstan-ignore-next-line array_filter+strlen */ 
		$acceptEncodingValues = \array_filter(\array_map('trim', \explode(',', $acceptEncodings)), 'strlen');
		$acceptEncodingValues[] = 'identity';
		$acceptEncodingValues = \array_unique($acceptEncodingValues);
		
		try
		{
			$request = $request->withHeader('Accept-Encoding', \implode(', ', $acceptEncodingValues));
		}
		catch(InvalidArgumentException $e)
		{
			// nothing  to do
		}
		
		return $request;
	}
	
	/**
	 * Post-processes the response by decompressing the data according to the
	 * headers that are given from the server.
	 *
	 * @param ResponseInterface $response
	 * @return ResponseInterface
	 * @todo use actual (un)zip library
	 * @throws RuntimeException
	 * @SuppressWarnings("PHPMD.ExcessiveMethodLength")
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 */
	protected function postProcess(ResponseInterface $response) : ResponseInterface
	{
		// unzip on the fly for files that are not on the filesystem
		if($response->hasHeader('Content-Encoding')
			&& !$response->hasHeader('X-Php-Download-File')
			&& !$response->hasHeader('X-Request-Header-X-Php-Download-File')
		) {
			$contentEncodings = $response->getHeader('Content-Encoding');
			
			foreach($contentEncodings as $contentEncoding)
			{
				$gzcontents = false;
				$contentEncoding = \mb_strtolower($contentEncoding);
				if(\in_array($contentEncoding, ['zip', 'x-zip'], true))
				{
					$gzcontents = \gzdecode($response->getBody()->__toString());
				}
				if(false !== $gzcontents)
				{
					// contents is successfully decoded, forge the new response
					$response = $this->setHeaderOrIgnore($response, 'Content-Encoding', 'identity');
					
					return $this->setBodyOrIgnore($response, $this->_streamFactory->createStream($gzcontents));
				}
			}
			
			return $response;
		}
		
		// if the file was put on the filesystem, then unzip it
		$tempBanZipFileName = $response->getHeaderLine('X-Request-Header-X-Php-Download-File');
		if(empty($tempBanZipFileName))
		{
			$tempBanZipFileName = $response->getHeaderLine('X-Php-Download-File');
		}
		
		// ... but if it's not, do nothing
		if(empty($tempBanZipFileName) || !\is_file($tempBanZipFileName))
		{
			return $response;
		}
		
		$requestUri = $response->getHeaderLine('X-Request-Uri');
		if(empty($requestUri))
		{
			// the underlying client does not support return of request uri
			return $response;
		}
		
		$tempBanUnzippedFileName = \dirname($tempBanZipFileName).\DIRECTORY_SEPARATOR.\sha1($requestUri);
		
		if(\is_file($tempBanUnzippedFileName))
		{
			$res = \unlink($tempBanUnzippedFileName);
			if(false === $res)
			{
				$message = 'Failed to remove old file at {path} for url {url}';
				$context = ['{path}' => $tempBanUnzippedFileName, '{url}' => $requestUri];
				
				throw new RuntimeException(\strtr($message, $context));
			}
		}
		
		$zip = new ZipArchive();
		$err = $zip->open($tempBanZipFileName);
		if(true !== $err)
		{
			$message = 'Failed to open resource as zip file at {path} (err {nb})';
			$context = ['{path}' => $tempBanZipFileName, '{nb}' => $err];
			
			throw new RuntimeException(\strtr($message, $context));
		}
		
		$outfile = \fopen($tempBanUnzippedFileName, 'w');
		if(false === $outfile)
		{
			$message = 'Failed to open destination file at {path}';
			$context = ['{path}' => $tempBanUnzippedFileName];
			
			throw new RuntimeException(\strtr($message, $context));
		}
		
		for($index = 0; $index < $zip->numFiles; $index++)
		{
			$entryName = $zip->getNameIndex($index);
			if(false === $entryName)
			{
				continue;
			}
			
			$entryStream = $zip->getStream($entryName);
			if(false === $entryStream)
			{
				continue;
			}
			
			$status = \stream_copy_to_stream($entryStream, $outfile);
			if(false === $status)
			{
				$message = 'Failed to unzip bytes from {zf} to {csvf}';
				$context = ['{zf}' => $tempBanZipFileName, '{csvf}' => $tempBanUnzippedFileName];
				
				throw new RuntimeException(\strtr($message, $context));
			}
		}
		
		$zip->close();
		\fclose($outfile);
		\unlink($tempBanZipFileName);
		
		if(\is_readable($tempBanUnzippedFileName))
		{
			$response = $this->setHeaderOrIgnore($response, 'X-Php-Uncompressed-File', $tempBanUnzippedFileName);
			$response = $this->setHeaderOrIgnore($response, 'Content-Type', 'identity');
			
			try
			{
				return $this->setBodyOrIgnore($response, $this->_streamFactory->createStreamFromFile($tempBanUnzippedFileName));
			}
			// @codeCoverageIgnoreStart
			catch(InvalidArgumentException $exc)
			{
				// should not happen
				throw new RuntimeException('Failed to build body with invalid mode', -1, $exc);
			}
			// @codeCoverageIgnoreEnd
		}
		
		$message = 'Failed to read extracted file at {path}';
		$context = ['{path}' => $tempBanUnzippedFileName];
		
		throw new RuntimeException(\strtr($message, $context));
	}
	
	/**
	 * Tries to set the header of the response, or ignore on failure.
	 *
	 * @param ResponseInterface $response
	 * @param string $header
	 * @param string $value
	 * @return ResponseInterface
	 */
	protected function setHeaderOrIgnore(ResponseInterface $response, string $header, string $value) : ResponseInterface
	{
		try
		{
			return $response->withHeader($header, $value);
		}
		catch(InvalidArgumentException $exc)
		{
			return $response;
		}
	}
	
	/**
	 * Tries to set the body of the response or ignore on failure.
	 *
	 * @param ResponseInterface $response
	 * @param StreamInterface $body
	 * @return ResponseInterface
	 */
	protected function setBodyOrIgnore(ResponseInterface $response, StreamInterface $body) : ResponseInterface
	{
		try
		{
			return $response->withBody($body);
		}
		catch(InvalidArgumentException $exc)
		{
			return $response;
		}
	}
	
}
