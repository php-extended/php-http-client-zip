<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-http-client-zip library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\HttpClient\ZipClientException;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * ZipClientExceptionTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\HttpClient\ZipClientException
 *
 * @internal
 *
 * @small
 */
class ZipClientExceptionTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ZipClientException
	 */
	protected ZipClientException $_object;
	
	public function testToString() : void
	{
		$this->assertStringContainsString(ZipClientException::class, $this->_object->__toString());
	}
	
	public function testGetRequest() : void
	{
		$this->assertInstanceOf(RequestInterface::class, $this->_object->getRequest());
	}
	
	public function testGetResponse() : void
	{
		$this->assertInstanceOf(ResponseInterface::class, $this->_object->getResponse());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ZipClientException($this->createMock(RequestInterface::class), $this->createMock(ResponseInterface::class), 'Message');
	}
	
}
